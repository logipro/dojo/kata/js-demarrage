Pour démarrer un projet en commonJS

# Installation

```bash
bin/npm install
```

# Tests

Tests unitaires
```bash
bin/jest
```

Vérfication de code et tests
```bash
./codecheck
```


